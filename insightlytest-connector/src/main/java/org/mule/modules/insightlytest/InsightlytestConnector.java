package org.mule.modules.insightlytest;

import org.mule.api.annotations.Config;
import org.mule.api.annotations.Configurable;
import org.mule.api.annotations.Connector;
import org.mule.api.annotations.Processor;

import java.io.IOException;

import org.mule.api.annotations.ReconnectOn;
import org.mule.api.annotations.param.Optional;
import org.mule.api.annotations.param.Payload;
import org.mule.api.annotations.rest.HttpMethod;
import org.mule.api.annotations.rest.RestCall;
import org.mule.api.annotations.rest.RestUriParam;
import org.mule.modules.insightlytest.config.ConnectorConfig;

@Connector(name="insightlytest", friendlyName="Insightlytest")
public abstract class InsightlytestConnector {
	
	@Config
    ConnectorConfig config;

    /**
     * Custom processor
     *
     * @param friend Name of a friend we want to greet
     * @return The greeting and reply to the selected friend.
     * @throws IOException Comment for Exception
     */
    @SuppressWarnings("deprecation")
	@Processor
    @ReconnectOn(exceptions = { Exception.class })
    @RestCall(uri="https://api.insight.ly/v2.1/{resourceName}", method=HttpMethod.GET)
    public abstract String retreive(@RestUriParam("resourceName") String resourceName) throws IOException;
    
    @SuppressWarnings("deprecation")
	@Processor
    @ReconnectOn(exceptions = { Exception.class })
    @RestCall(uri="https://api.insight.ly/v2.1/{resourceName}", method=HttpMethod.POST)
    public abstract String create(@RestUriParam("resourceName") String resourceName, @Payload String payload) throws IOException;

    public ConnectorConfig getConfig() {
        return config;
    }

    public void setConfig(ConnectorConfig config) {
        this.config = config;
    }

}