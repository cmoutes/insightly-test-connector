
package org.mule.modules.insightlytest.generated.adapters;

import javax.annotation.Generated;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.devkit.ProcessAdapter;
import org.mule.api.devkit.ProcessTemplate;
import org.mule.api.processor.MessageProcessor;
import org.mule.api.routing.filter.Filter;
import org.mule.modules.insightlytest.InsightlytestConnector;
import org.mule.security.oauth.callback.ProcessCallback;


/**
 * A <code>InsightlytestConnectorProcessAdapter</code> is a wrapper around {@link InsightlytestConnector } that enables custom processing strategies.
 * 
 */
@SuppressWarnings("all")
@Generated(value = "Mule DevKit Version 3.8.0", date = "2016-02-09T03:23:54-05:00", comments = "Build UNNAMED.2762.e3b1307")
public abstract class InsightlytestConnectorProcessAdapter
    extends InsightlytestConnectorLifecycleInjectionAdapter
    implements ProcessAdapter<InsightlytestConnectorCapabilitiesAdapter>
{


    public<P >ProcessTemplate<P, InsightlytestConnectorCapabilitiesAdapter> getProcessTemplate() {
        final InsightlytestConnectorCapabilitiesAdapter object = this;
        return new ProcessTemplate<P,InsightlytestConnectorCapabilitiesAdapter>() {


            @Override
            public P execute(ProcessCallback<P, InsightlytestConnectorCapabilitiesAdapter> processCallback, MessageProcessor messageProcessor, MuleEvent event)
                throws Exception
            {
                return processCallback.process(object);
            }

            @Override
            public P execute(ProcessCallback<P, InsightlytestConnectorCapabilitiesAdapter> processCallback, Filter filter, MuleMessage message)
                throws Exception
            {
                return processCallback.process(object);
            }

        }
        ;
    }

}
