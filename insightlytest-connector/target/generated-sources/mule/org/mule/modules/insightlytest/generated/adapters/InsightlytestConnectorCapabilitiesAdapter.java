
package org.mule.modules.insightlytest.generated.adapters;

import javax.annotation.Generated;
import org.mule.api.devkit.capability.Capabilities;
import org.mule.api.devkit.capability.ModuleCapability;
import org.mule.modules.insightlytest.InsightlytestConnector;


/**
 * A <code>InsightlytestConnectorCapabilitiesAdapter</code> is a wrapper around {@link InsightlytestConnector } that implements {@link org.mule.api.Capabilities} interface.
 * 
 */
@SuppressWarnings("all")
@Generated(value = "Mule DevKit Version 3.8.0", date = "2016-02-09T03:23:54-05:00", comments = "Build UNNAMED.2762.e3b1307")
public abstract class InsightlytestConnectorCapabilitiesAdapter
    extends InsightlytestConnector
    implements Capabilities
{


    /**
     * Returns true if this module implements such capability
     * 
     */
    public boolean isCapableOf(ModuleCapability capability) {
        if (capability == ModuleCapability.LIFECYCLE_CAPABLE) {
            return true;
        }
        return false;
    }

}
